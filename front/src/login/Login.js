import React from "react";

export default class Login extends React.Component {

    emptyUser = {
        userName: '',
        password: '',
    };

    constructor(props) {
        super(props);
        this.state = {
            user: this.emptyUser,
        };
    }

    handleSubmit = async (event) => {
        event.preventDefault();

        const {user} = this.state;

        const re = await fetch('http://localhost:8080/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user),
        }).then(response => {
                response.json().then(user => {
                    this.props.changeAuthenticatedStatus('true', user);
                    this.props.hideForm();
                })
            }
        );
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let user = {...this.state.user};
        user[name] = value;
        this.setState({user});
    }

    render() {
        const {user} = this.state;
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="userName">Enter userName</label>
                    <input id="userName" name="userName" type="text"
                           value={user.userName || ''} onChange={this.handleChange}/>
                    <label htmlFor="password">Enter password</label>
                    <input id="password" name="password" type="text"
                           value={user.password || ''} onChange={this.handleChange}/>
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}