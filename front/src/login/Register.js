import React, {useState} from "react";
import "./Login.css";

export default class Register extends React.Component {

    emptyUser = {
        firstName: '',
        secondName: '',
        userName: '',
        password: '',
        birthDate: '',
        email: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            user: this.emptyUser
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let user = {...this.state.user};
        user[name] = value;
        this.setState({user});
    }

    handleSubmit = async (event) => {

        const {user} = this.state;


        await fetch('http://localhost:8080/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user),
        });
        this.props.changeFormView("register", false);
    }

    render() {
        const {user} = this.state;
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="firstName">Enter Firstname</label>
                    <input id="firstName" name="firstName" type="text"
                           value={user.firstName || ''} onChange={this.handleChange}/>
                    <label htmlFor="secondName">Enter Lastname</label>
                    <input id="secondName" name="secondName" type="text"
                           value={user.secondName || ''} onChange={this.handleChange}/>
                    <label htmlFor="userName">Enter userName</label>
                    <input id="userName" name="userName" type="text"
                           value={user.userName || ''} onChange={this.handleChange}/>
                    <label htmlFor="password">Enter password</label>
                    <input id="password" name="password" type="text"
                           value={user.password || ''} onChange={this.handleChange}/>
                    <label htmlFor="birthDate">Enter birthDate</label>
                    <input id="birthDate" name="birthDate" type="text"
                           value={user.birthDate || ''} onChange={this.handleChange}/>
                    <label htmlFor="email">Enter email</label>
                    <input id="email" name="email" type="email"
                           value={user.email || ''} onChange={this.handleChange}/>
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}