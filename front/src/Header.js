import React from "react";

export default class Header extends React.Component {

    constructor(props) {
        super(props);
    }

    viewForm = (e) => {
        this.props.changeFormView(e.target.className, true);
    }

    hideForm = () => {
        this.props.hideForm();
    }

    logout = () => {
        this.props.logout();
    }

    render() {
        const backButton = this.props.isAuthenticated ? null : <ul className="nav navbar-nav">
            <button onClick={this.hideForm} className="back">Back</button>
        </ul>
        const loginButton = this.props.isAuthenticated ? null : <ul className="nav navbar-nav">
            <button onClick={this.viewForm} className="login">Login</button>
        </ul>;

        const registerButton = this.props.isAuthenticated ? null : <ul className="nav navbar-nav navbar-right">
            <button onClick={this.viewForm} className="register">Register</button>
        </ul>;
        const logOutButton = this.props.isAuthenticated ? <ul className="nav navbar-nav navbar-right">
            <button onClick={this.logout} className="logout">Log out</button>
        </ul> : null;

        return (
            <nav className="navbar navbar-default navbar-static-top">
                <div className="container">
                    <div id="navbar-collapse" className="collapse navbar-collapse">

                        {backButton}
                        {loginButton}
                        {registerButton}
                        {logOutButton}
                    </div>
                </div>
            </nav>
        )
    }

}