import React from "react";
import Register from './login/Register'
import Login from './login/Login'
import Header from "./Header";

let isAuthenticated = false;
export default class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            register: false,
            login: false,
            isAuthenticated: isAuthenticated,
            user: null
        }
    }

    componentDidMount() {
        if (isAuthenticated) {
            this.setState({isAuthenticated: true})
        }
    }

    changeAuthenticatedStatus = (status, user = null) => {
        isAuthenticated = true;
        this.setState({isAuthenticated: status, user: JSON.stringify(user)});
    }

    changeFormView = (formName, value) => {
        const formNameToHide = formName === "register" ? "login" : "register";
        this.setState({[formName]: [value], [formNameToHide]: false});
    }

    hideForm = () => {
        this.setState({
            register: false,
            login: false,
        });
    }

    logout = () => {
        this.setState({
            register: false,
            login: false,
            isAuthenticated: false,
            user: null
        })
    }

    render() {
        let user = JSON.parse(this.state.user);
        const isAuthenticated = this.state.isAuthenticated ?
            <div>You are logged as {user.firstName} {user.secondName}</div> : null;

        let registerForm = this.state.register ? <Register changeAuthenticatedStatus={this.changeAuthenticatedStatus}
                                                           changeFormView={this.changeFormView}/> : null;
        let loginForm = this.state.login ?
            <Login hideForm={this.hideForm} changeAuthenticatedStatus={this.changeAuthenticatedStatus}
                   changeFormView={this.changeFormView}/> : null;
        return (
            <div>
                <Header isAuthenticated={this.state.isAuthenticated} changeFormView={this.changeFormView}
                        hideForm={this.hideForm} logout={this.logout}/>
                {isAuthenticated}
                {registerForm}
                {loginForm}
                <footer></footer>
            </div>
        )
    }
}