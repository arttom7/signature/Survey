package com.example.backend.user;

import com.example.backend.user.model.LoginUser;
import com.example.backend.user.model.User;
import com.example.backend.user.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDtoMapper userDtoMapper;

    public User createUser(UserDto newUser) {
        User userToSave = buildUser(newUser);

        return userRepository.save(userToSave);
    }

    private User buildUser(UserDto newUser) {
        return User.builder()
                .firstname(newUser.getFirstName())
                .lastname(newUser.getSecondName())
                .userName(newUser.getUserName())
                .password(newUser.getPassword())
                .birthDate(newUser.getBirthDate())
                .email(newUser.getEmail())
                .build();
    }

    public UserDto getUser(LoginUser loginUser) {
        Optional<User> user = userRepository.getUserByPasswordAndUserName(loginUser.getPassword(), loginUser.getUserName());
        return userDtoMapper.apply(user.get());
    }
}
