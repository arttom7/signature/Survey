package com.example.backend.user;

import com.example.backend.user.model.LoginUser;
import com.example.backend.user.model.User;
import com.example.backend.user.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(path = "/user")
    public ResponseEntity<User> createUser(@RequestBody UserDto newUser) throws URISyntaxException {
        User createdUser = userService.createUser(newUser);
        return ResponseEntity.created(new URI("/user/" + createdUser.getId()))
                .body(createdUser);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<UserDto> signInUser(@RequestBody LoginUser loginUser) throws URISyntaxException {
        UserDto loggedUser = userService.getUser(loginUser);
        return ResponseEntity.ok().body(loggedUser);
    }

}
