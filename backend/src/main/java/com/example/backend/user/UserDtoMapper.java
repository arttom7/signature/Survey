package com.example.backend.user;

import com.example.backend.user.model.User;
import com.example.backend.user.model.UserDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserDtoMapper implements Function<User, UserDto> {
    @Override
    public UserDto apply(User user) {
        return UserDto.builder()
                .firstName(user.getFirstname())
                .secondName(user.getLastname())
                .birthDate(user.getBirthDate())
                .userName(user.getUserName())
                .email(user.getEmail())
                .password(user.getPassword())
                .build();
    }
}
