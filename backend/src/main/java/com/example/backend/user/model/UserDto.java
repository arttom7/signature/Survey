package com.example.backend.user.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDto {
    private String firstName;
    private String secondName;
    private String password;
    private String userName;
    private String birthDate;
    private String email;

}
