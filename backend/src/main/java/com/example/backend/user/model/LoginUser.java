package com.example.backend.user.model;

import lombok.Data;

@Data
public class LoginUser {
    private String password;
    private String userName;
}
